import React from 'react';
import styles from './SocialContent.module.scss';
import firebase from '../firebase.config';
import Button from "../components/Button";
import classnames from 'classnames';
import { docData } from 'rxfire/firestore';

export default class SocialContent extends React.Component {

  state = {
    contentData: null,
    inputName: '',
    inputText: '',
    showInput: false,
  };

  async componentDidMount() {
    const contentRef = firebase.firestore().collection('content').doc(this.props.data.id);
    docData(contentRef).subscribe(doc => {
      this.setState({ contentData: doc });
    });
  }

  createMessages = () => {
    let messages = this.state.contentData.messages.sort((m1, m2) => new Date(m2.createdAt.toDate()) - new Date(m1.createdAt.toDate()));
    if (!this.props.mobile) {
      messages = messages.slice(0,3);
    }
    return messages.map((message, index) => {
      const createdAt = new Date(message.createdAt.toDate());
      return (
        <div className={styles.messageBox} key={'message_' + index}>
          <div className={styles.senderImage}><img src={"/assets/no_user.png"} /></div>
          <div className={styles.info}>
            <div className={styles.message}>{message.message}</div>
            <div className={styles.sender}>{message.sender}</div>
            <div className={styles.createdAt}>{createdAt.getDate() + '.' + (createdAt.getMonth() + 1) + '.' + createdAt.getFullYear()}</div>
          </div>
        </div>
      )
    });
  };

  sendMessage = async () => {
    if (this.state.inputText === '') {
      this.setState({ showInput: false });
      return;
    }
    const messages = this.state.contentData.messages;
    messages.push({
      sender: this.state.inputName,
      message: this.state.inputText,
      likes: 0,
      createdAt: new Date(),
    });
    const contentRef = firebase.firestore().collection('content').doc(this.props.data.id);
    await contentRef.update({ messages });

    const contentData = this.state.contentData;
    contentData.messages = messages;
    this.setState({ contentData, inputText: '', inputName: '', showInput: false });
  };

  render() {
    const components = [];

    if (this.props.mobile) {
      const inputClass = classnames(styles.input, {[styles.show]: this.state.showInput});
      components.push(
        <>
          <div className={inputClass} key={"input"}>
            <label>
              Name:
              <input type={"text"} placeholder={"Name"} value={this.state.inputName} onChange={event => this.setState({ inputName: event.target.value })}/>
            </label>
            <label>
              Message:
              <textarea placeholder={"Message"} value={this.state.inputText} onChange={event => this.setState({ inputText: event.target.value })} />
            </label>
            <Button value={"Submit"} onClick={this.sendMessage} />
          </div>
          { !this.state.showInput &&
            <div className={styles.sendMessage} onClick={() => this.setState({showInput: !this.state.showInput})}>
              { this.state.showInput && 'Close' }
              { !this.state.showInput && 'Send Message' }
            </div>
          }
        </>
      );
    }

    if (this.state.contentData) {
      components.push(
        <div className={styles.socialContent} key={"content"}>
          <div className={styles.left}>
            { this.createMessages() }
          </div>
          <div className={styles.right}>
            <img src={'/assets/energy_consumption.png'} />
          </div>
        </div>
      );
    } else {
      components.push(
        <div className={styles.socialContent} key={"content"}>

        </div>
      )
    }
    return components;
  }
}
