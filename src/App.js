import React from 'react';
import Companion from "./Companion";
import Frame from "./Frame";
import {
  BrowserRouter as Router,
  Route, Switch,
} from 'react-router-dom';
import Reader from "./components/Reader";

export default class App extends React.Component {

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path={'/'} component={Companion} />
          <Route exact path={'/frame/:id'} component={Frame} />
          <Route exact path={'/:frameId/:contentId'} component={Companion} />
          <Route exact path={'/reader'} component={Reader} />
        </Switch>
      </Router>
    );
  }
}
