import React from 'react';
import styles from './Time.module.scss';

export default class Time extends React.Component {

  state = {
    timeString: '',
    dateString: '',
  };

  componentDidMount() {
    setInterval(() => {
      const date = new Date();
      const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
      const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
      const timeString = hours + ':' + minutes;
      const dateString = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
      this.setState({ timeString, dateString });
    }, 1000);

  }

  render() {
    return (
      <div className={styles.time}>
        <div className={styles.left}>
          <div className={styles.weather}><img src={'/assets/date_n_weather.png'}/></div>
        </div>
        <div className={styles.right}>
          <div className={styles.timeString}>{ this.state.timeString }</div>
          <div className={styles.addressString}>{ this.props.address }</div>
        </div>
      </div>
    );
  }
}
