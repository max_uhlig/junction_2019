import React from 'react';
import styles from './FidCode.module.scss';
import QRCode from 'qrcode.react';
import Isvg from 'react-inlinesvg';
import classnames from 'classnames';

export default class FidCode extends React.Component {

  render() {
    const value = JSON.stringify({
      f: this.props.frameId,
      c: this.props.contentId,
    });
    const fidClass = classnames(styles.fidCode, {
      [styles.light]: this.props.light,
      [styles.dark]: this.props.dark
    });
    return (
      <div className={fidClass}>
        <Isvg cacheRequests src={'/assets/fid_code_v01.svg'} />
        <div className={styles.qrCode}>
          <QRCode value={value} style={{ width: 100, height: 100 }}/>
        </div>
      </div>
    )
  }
}
