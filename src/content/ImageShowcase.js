import React from 'react';
import styles from './ImageShowcase.module.scss';
import Isvg from 'react-inlinesvg';

export default class ImageShowcase extends React.Component {

  render() {
    const image = this.props.mobile ? this.props.data.imageMobile : this.props.data.image;
    const imageBackground = {
      backgroundImage: 'url(/assets/' + image + ')',
    };
    return (
      <div className={styles.imageShowcase}>
        <div className={styles.left}>
          <div className={styles.title}>{ this.props.data.title }</div>
          <div className={styles.name}>{ this.props.data.name }</div>
          <div className={styles.apartment}>
            <Isvg cacheRequests src={'/assets/apartment.svg'} />
            { this.props.data.apartmentNbr }
          </div>
          <div className={styles.instagram}>
            <Isvg cacheRequests src={'/assets/instagram.svg'} />
            { this.props.data.instagramHandle }
          </div>
          <div className={styles.description}>{this.props.data.description}</div>
        </div>
        <div className={styles.right} style={imageBackground}>
        </div>
      </div>
    )
  }
}
