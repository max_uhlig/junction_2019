import firebase from "firebase/app";
import "firebase/firestore";

const firebaseData = {
  dev: {
    apiKey: "AIzaSyDGkQMkXsmdq1I27ucKXzif22hx_WmBhxY",
    authDomain: "junction2019-88ad7.firebaseapp.com",
    databaseURL: "https://junction2019-88ad7.firebaseio.com",
    projectId: "junction2019-88ad7",
    storageBucket: "junction2019-88ad7.appspot.com",
    messagingSenderId: "652615394232",
    appId: "1:652615394232:web:0f5378ead082de1d99a54d"
  },
};

const env = firebaseData['dev'];

if (!firebase.apps.length) {
  firebase.initializeApp(env);
}

export default firebase;
