import React from 'react';
import styles from './Frame.module.scss';
import firebase from './firebase.config';
import FidCode from "./components/FidCode";
import ImageShowcase from "./content/ImageShowcase";
import StaticContent from "./content/StaticContent";
import SocialContent from "./content/SocialContent";
import TopBar from "./components/TopBar";

export default class Frame extends React.Component {

  state = {
    frameData: null,
    frameId: '',
    contentId: '',
    contentData: null,
    contentList: [],
    index: 0,
    theme: 'light',
  };

  async componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      const screen = await firebase.firestore().collection('frames').doc(id).get();
      if (screen.exists) {
        const frameData = screen.data();
        this.setState({ frameData, frameId: id });
        const contentIds = frameData.contentIds;
        contentIds.forEach(async id => {
          const contentList = this.state.contentList;
          const contentDoc = await firebase.firestore().collection('content').doc(id).get();
          if (contentDoc.exists) {
            const data = contentDoc.data();
            data.id = contentDoc.id;
            contentList.push(data);
            this.setState({contentList});
          }
        });
      }
    }

    setInterval(() => {
      let index = this.state.index;
      index = index + 1 === this.state.contentList.length ? 0 : index + 1;
      const content = this.state.contentList[index];
      this.setState({ index, contentId: content.id, theme: content.theme });
    }, 5000);
  }

  createContentList = () => {
    const list = [];
    this.state.contentList.forEach(content => {
      if (content.contentType === 'imageShowcase') {
        list.push(
          <ImageShowcase key={content.id} data={content} />
        )
      }
      if (content.contentType === 'svgContent') {
        list.push(
          <StaticContent key={content.id} data={content} svg />
        );
      }
      if (content.contentType === 'socialContent') {
        list.push(
          <SocialContent key={content.id} data={content} />
        );
      }
      if (content.contentType === 'jpgContent') {
        list.push(
          <StaticContent key={content.id} data={content} jpg />
        )
      }
    });
    return list;
  };

  render() {
    if (this.state.frameData) {
      const contentList = this.createContentList();
      const sliderStyle = {
        left: (this.state.index * 100 * -1) + 'vw'
      };

      return (
        <div className={styles.frame}>
          { /* <Time address={this.state.frameData.address} /> */ }
          <TopBar address={this.state.frameData.address} />
          <div className={styles.slider} style={sliderStyle}>{ contentList }</div>
          <FidCode frameId={this.state.frameId} contentId={this.state.contentId} light={this.state.theme === 'light'} dark={this.state.theme === 'dark'}/>
        </div>
      );
    } else {
      return (
        <div className={styles.screen}>

        </div>
      )
    }

  }
}
