import React from 'react';
import styles from './TopBar.module.scss';
import Time from "./Time";

export default class TopBar extends React.Component {

  render() {

    return (
      <div className={styles.topBar}>
        <div className={styles.info}>
          <div className={styles.subway}><img src={'/assets/subway_info.png'}/></div>
          <div className={styles.sauna}><img src={'/assets/sauna_info.png'}/></div>
        </div>
        { /* <Isvg cacheRequests src={'/assets/top_bar_flex-sauna.svg'} /> */ }
        <Time address={this.props.address} />
      </div>
    )
  }
}
