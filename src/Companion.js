import React from 'react';
import styles from './Companion.module.scss';
import Button from "./components/Button";
import { Link } from 'react-router-dom';
import firebase from './firebase.config';
import ImageShowcase from "./content/ImageShowcase";
import StaticContent from "./content/StaticContent";
import SocialContent from "./content/SocialContent";

export default class Companion extends React.Component {

  state = {
    frameData: null,
    contentId: false,
    contentList: [],
    index: 0,
  };

  async componentDidMount() {
    const frameId = this.props.match.params.frameId;
    const contentId = this.props.match.params.contentId;

    // console.log(frameId, contentId);
    if (frameId && contentId) {
      const frameDoc = await firebase.firestore().collection('frames').doc(frameId).get();
      const frameData = frameDoc.data();
      frameData.id = frameDoc.id;
      this.setState({ frameData: frameData });

      const contentList = this.state.contentList;
      await Promise.all(frameData.contentIds.map(async id => {
        const contentDoc = await firebase.firestore().collection('content').doc(id).get();
        const contentData = contentDoc.data();
        contentData.id = contentDoc.id;
        contentList.push(contentData);
      }));
      const index = contentList.findIndex(c => c.id === contentId);
      this.setState({ contentList, index });
    }
  }

  goLeft = () => {
    const index = this.state.index - 1 < 0 ? this.state.contentList.length - 1 : this.state.index - 1;
    this.setState({ index });
  };

  goRight = () => {
    const index = this.state.index + 1 === this.state.contentList.length ? 0 : this.state.index + 1;
    this.setState({ index });
  };

  render() {
    if (this.state.contentList.length === 0) {
      return (
        <div className={styles.companion}>
          <h1>Welcome to Domo Arigato</h1>
          <Link to={'/reader'}>
            <Button value={'Scan Screen'}/>
          </Link>
        </div>
      )
    } else {
      const content = this.state.contentList[this.state.index];
      let contentComponent = false;
      if (content.contentType === 'imageShowcase') {
        contentComponent = <ImageShowcase data={content} mobile />;
      }
      if (content.contentType === 'svgContent') {
        contentComponent = <StaticContent data={content} mobile svg />;
      }
      if (content.contentType === 'jpgContent') {
        contentComponent = <StaticContent data={content} mobile jpg />
      }
      if (content.contentType === 'socialContent') {
        contentComponent = <SocialContent data={content} mobile />
      }
      return (
        <div className={styles.companion}>
          <div className={styles.nav}>
            <div className={styles.left} onClick={this.goLeft}>Left</div>
            <div className={styles.right} onClick={this.goRight}>Right</div>
          </div>
          { contentComponent }
        </div>
      )
    }
  }
}
