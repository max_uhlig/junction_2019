import React from 'react';
import styles from './Button.module.scss';

export default class Button extends React.Component {

  onClick = () => {
    if (this.props.onClick) {
      this.props.onClick();
    }
  };

  render() {

    return (
      <div className={styles.button} onClick={this.onClick}>
        { this.props.value }
      </div>
    )
  }
}
