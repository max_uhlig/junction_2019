import React from 'react';
import styles from './StaticContent.module.scss';
import Isvg from 'react-inlinesvg';
import classnames from 'classnames';

export default class StaticContent extends React.Component {

  render() {
    const image = this.props.mobile ? this.props.data.imageMobile : this.props.data.image;
    const contentClasses = classnames(styles.staticContent, {[styles.mobile]: this.props.mobile});
    return (
      <div className={contentClasses}>
        { this.props.svg &&
          <Isvg cacheRequests src={'/assets/' + image}/>
        }
        { this.props.jpg &&
          <img src={'/assets/' + image } />
        }
      </div>
    )
  }
}
