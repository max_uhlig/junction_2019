import React from 'react';
import styles from './Reader.module.scss';
import {Link} from "react-router-dom";
import Button from "./Button";
let QrReader;

export default class Reader extends React.Component {

  state = {
    showScanner: false,
    data: false,
    showLink: false,
  };

  componentDidMount() {
    QrReader = require('react-qr-reader');
    this.setState({ showScanner: true });
  }

  handleScan = (scan) => {
    if (scan) {
      const data = JSON.parse(scan);
      window.location = '/' + data.f + '/' + data.c;
      this.setState({data, showLink: true });
    }
  };

  handleError = (error) => {
    this.setState({error});
  };

  render() {

    return (
      <div className={styles.reader}>
        <Link to={'/'}>
          <Button value={'back'} />
        </Link>
        { this.state.showScanner &&
          <QrReader
            delay={300}
            onError={this.handleError}
            onScan={this.handleScan}
            style={{ width: '100%' }}
          />
        }
        { this.state.data && JSON.stringify(this.state.data) }
        { this.state.error && 'some error happened...' }
      </div>
    )
  }
}
